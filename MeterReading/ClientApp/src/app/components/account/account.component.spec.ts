import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AccountComponent } from './account.component';

describe('AccountComponent', () => {
  let component: AccountComponent;

  const id = 'id';
  const http = jasmine.createSpyObj('HttpClient', ['get']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AccountComponent],
      imports: [RouterTestingModule],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of({ get: _ => id })
          }
        },
        { provide: HttpClient, useValue: http }
      ]
    });

    component = TestBed.createComponent(AccountComponent).componentInstance;
  });

  it('requests account with id from router params', (done) => {
    http.get.and.returnValue(of({}));

    component.ngOnInit();

    component.account$.subscribe({
      next: () => {
        expect(http.get).toHaveBeenCalledWith(`/api/accounts/${id}`)
      },
      complete: done,
    })
  });

  it('returns response for account', (done) => {
    const expectedAccount = {
      id: 1234,
      firstName: 'Freya',
      lastName: 'Test'
    };

    http.get.and.returnValue(of(expectedAccount));

    component.ngOnInit();

    component.account$.subscribe({
      next: account => expect(account).toBe(expectedAccount),
      complete: done,
    })
  });

  it('requests readings with id from router params', (done) => {
    http.get.and.returnValue(of([]));

    component.ngOnInit();

    component.readings$.subscribe({
      next: () => {
        expect(http.get).toHaveBeenCalledWith(
          `/api/accounts/${id}/readings`
        )
      },
      complete: done,
    })
  });

  it('returns readings sorted by date', (done) => {
    const expectedReadings = [
      {
        accountID: 1,
        date: '2019-04-22 12:25:00',
        value: 12
      },
      {
        accountID: 2,
        date: '2019-04-22 09:24:00',
        value: 21
      },
    ];

    http.get.and.returnValue(of(expectedReadings));

    component.ngOnInit();

    component.readings$.subscribe({
      next: readings => {
        expect(readings).toEqual(expectedReadings.sort((a, b) =>
          new Date(a.date).getTime() - new Date(b.date).getTime()
        ))
      },
      complete: done,
    })
  });
});