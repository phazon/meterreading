import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { flatMap, map, shareReplay } from 'rxjs/operators';
import { AccountModel } from '../accounts/accounts.component';
import { ReadingModel } from '../readings/readings.component';

@Component({
  selector: 'account',
  templateUrl: './account.component.html'
})
export class AccountComponent implements OnInit {
  public account$: Observable<AccountModel>;
  public readings$: Observable<ReadingModel[]>;

  constructor(
    private route: ActivatedRoute,
    private http: HttpClient
  ) { }

  public ngOnInit(): void {
    const accountId$ = this.route.paramMap.pipe(
      map(params => params.get('id')),
      shareReplay()
    );

    this.account$ = accountId$.pipe(
      flatMap(accountId => this.http.get<AccountModel>(
        `/api/accounts/${accountId}`
      ))
    );

    this.readings$ = accountId$.pipe(
      flatMap(accountId => this.http.get<ReadingModel[]>(
        `/api/accounts/${accountId}/readings`
      )),
      map(readings => readings.sort((a, b) =>
        new Date(a.date).getTime() - new Date(b.date).getTime()
      ))
    );
  }
}