import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ReadingsComponent } from './readings.component';

describe('ReadingsComponent', () => {
  let component: ReadingsComponent;

  let http = jasmine.createSpyObj('HttpClient', ['get']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ReadingsComponent
      ],
      imports: [RouterTestingModule],
      providers: [
        { provide: HttpClient, useValue: http }
      ]
    });

    component = TestBed.createComponent(ReadingsComponent)
      .componentInstance;
  });

  it('returns readings sorted by date', (done) => {
    const expectedReadings = [
      {
        accountID: 1,
        date: '2019-04-22 12:25:00',
        value: 12
      },
      {
        accountID: 2,
        date: '2019-04-22 09:24:00',
        value: 21
      },
    ];

    http.get.and.returnValue(of(expectedReadings));

    component.ngOnInit();

    component.readings$.subscribe({
      next: readings => {
        expect(readings).toEqual(expectedReadings.sort((a, b) =>
          new Date(a.date).getTime() - new Date(b.date).getTime()
        ))
      },
      complete: done,
    })
  });
});