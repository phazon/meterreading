import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

export class ReadingModel {
  public accountID: number;
  public date: string;
  public value: number;
}

@Component({
  selector: 'readings',
  templateUrl: './readings.component.html'
})
export class ReadingsComponent implements OnInit {
  public readings$: Observable<ReadingModel[]>;

  constructor(private http: HttpClient) { }

  public ngOnInit(): void {
    this.readings$ = this.http.get<ReadingModel[]>('/api/accounts/readings')
      .pipe(
        map(readings => readings.sort((a, b) =>
          new Date(a.date).getTime() - new Date(b.date).getTime()
        ))
      );
  }
}