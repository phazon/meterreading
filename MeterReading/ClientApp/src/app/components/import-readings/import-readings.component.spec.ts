import { HttpClient } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ErrorReason, ImportReadingsComponent } from './import-readings.component';

describe('ImportReadingsComponent', () => {
  let component: ImportReadingsComponent;

  let http = jasmine.createSpyObj('HttpClient', ['post']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ImportReadingsComponent
      ],
      providers: [
        { provide: HttpClient, useValue: http }
      ]
    }).compileComponents();;

    let fixture = TestBed.createComponent(ImportReadingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('posts contents of file on upload', (done) => {
    const csv = 'some csv';
    setCsv(csv);

    http.post.and.returnValue(of({
      successful: [],
      errors: []
    }));

    component.onUpload();

    component.upload$.subscribe({
      next: () => {
        expect(http.post).toHaveBeenCalledWith(
          '/api/accounts/readings',
          { csv }
        )
      },
      complete: done
    })
  });

  it('returns result with errors sorted by line', (done) => {
    setCsv('some csv');

    var success = [
        {
          line: 1,
          accountId: 1,
          meterReadingDateTime: '2019-04-22 09:24:00',
          meterReadValue: 20
        }
    ];

    var errors  = [
      { line: 3, reason: ErrorReason.InvalidEntry },
      { line: 2, reason: ErrorReason.InvalidEntry }
    ];

    http.post.and.returnValue(of({
      successful: success,
      errors: errors
    }));

    component.onUpload();

    component.upload$.subscribe({
      next: result => {
        expect(result.successful).toEqual(success);
        expect(result.errors).toEqual(errors.sort((a, b) => a.line - b.line));
      },
      complete: done
    })
  });

  function setCsv(csv: string) {
    const file = {
      name: 'csv_filename.csv',
      text: () => Promise.resolve(csv)
    };

    const fileList = {
      item: () => file
    };

    component.onChange(fileList as any);
  }
});