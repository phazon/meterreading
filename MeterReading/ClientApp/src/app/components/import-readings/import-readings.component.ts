import { Component, ViewChild, ElementRef } from '@angular/core';
import { Observable, from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { flatMap, map } from 'rxjs/operators';

class Entry {
  public line: number;
  public accountId: number;
  public meterReadingDateTime: string;
  public meterReadValue: number;
}

export enum ErrorReason {
  InvalidEntry,
  AlreadyExists,
  AccountNotFound
}

class Error {
  public line: number;
  public reason: ErrorReason;
}

class ImportReadingsResponse {
  public successful: Entry[];
  public errors: Error[];
}

@Component({
  selector: 'import-readings',
  templateUrl: './import-readings.component.html'
})
export class ImportReadingsComponent {
  @ViewChild('label', { static: false })
  public label: ElementRef;
  
  public upload$: Observable<ImportReadingsResponse>;

  private file: File;

  constructor(private http: HttpClient) { }

  public onChange(files: FileList): void {
    this.file = files.item(0);

    this.label.nativeElement.textContent = this.file.name;
  }

  public onUpload(): void {
    if (this.file === undefined) { return; }

    this.upload$ = from((this.file as any).text())
      .pipe(
        flatMap(csv => this.http.post<ImportReadingsResponse>(
          '/api/accounts/readings',
          { csv }
        )),
        map(result => ({
          ...result,
          errors: result.errors.sort((a, b) => a.line - b.line)
        }))
      );
  }

  public reasonText(reason: ErrorReason) {
    switch (reason) {
      case ErrorReason.InvalidEntry: return 'Invalid Entry';
      case ErrorReason.AlreadyExists: return 'Already Exists';
      case ErrorReason.AccountNotFound: return 'Account Not Found';
      default: return 'Unknown';
    }
  }
}