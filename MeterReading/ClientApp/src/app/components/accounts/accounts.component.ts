import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export class AccountModel {
  public id: number;
  public firstName: string;
  public lastName: string;
}

@Component({
  selector: 'accounts',
  templateUrl: './accounts.component.html'
})
export class AccountsComponent implements OnInit {
  public accounts$: Observable<AccountModel[]>;

  constructor(private http: HttpClient) { }

  public ngOnInit(): void {
    this.accounts$ = this.http.get<AccountModel[]>('/api/accounts');
  }
}