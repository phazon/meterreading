import { AccountsComponent } from './accounts.component';
import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

describe('AccountsComponent', () => {
  let component: AccountsComponent;

  let http = jasmine.createSpyObj('HttpClient', ['get']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AccountsComponent
      ],
      imports: [RouterTestingModule],
      providers: [
        { provide: HttpClient, useValue: http }
      ]
    });

    component = TestBed.createComponent(AccountsComponent)
      .componentInstance;
  });

  it('creates observable for API request', (done) => {
    http.get.and.returnValue(of([]));

    component.ngOnInit();

    component.accounts$.subscribe({
      next: accounts => {
        expect(http.get).toHaveBeenCalledWith('/api/accounts');
      },
      complete: done,
    });
  });

  it('returns accounts', (done) => {
    var expectedAccounts = [
      {
        id: 1,
        firstName: 'Terry',
        lastName: 'Test'
      },
      {
        id: 2,
        firstName: 'Johnny',
        lastName: 'Test'
      }
    ];

    http.get.and.returnValue(of(expectedAccounts));

    component.ngOnInit();

    component.accounts$.subscribe({
      next: accounts => {
        expect(accounts).toBe(expectedAccounts);
      },
      complete: done,
    })
  });
});