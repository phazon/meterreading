import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ReadingsComponent } from './components/readings/readings.component';
import { AccountComponent } from './components/account/account.component';
import { ImportReadingsComponent } from './components/import-readings/import-readings.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AccountsComponent,
    AccountComponent,
    ReadingsComponent,
    ImportReadingsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: AccountsComponent, pathMatch: 'full' },
      { path: 'account/:id', component: AccountComponent },
      { path: 'readings', component: ReadingsComponent },
      { path: 'import-readings', component: ImportReadingsComponent },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
