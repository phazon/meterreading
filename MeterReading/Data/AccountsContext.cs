﻿using MeterReading.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace MeterReading.Data
{

    public class AccountsContext : DbContext
    {
        private Action<DbContextOptionsBuilder> Builder { get; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<Reading> Readings { get; set; }

        public AccountsContext() : base() { }

        public AccountsContext(Action<DbContextOptionsBuilder> builder)
        {
            Builder = builder;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (Builder != null)
            {
                Builder(options);

                return;
            }

            options.UseSqlite("Data Source=accounts.db");
        }
    }
}
