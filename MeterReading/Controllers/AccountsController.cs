﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using LanguageExt;
using MeterReading.Models;
using MeterReading.Models.CSV.Reading;
using MeterReading.Services;

namespace MeterReading.Controllers
{
    public class AccountsController : ControllerBase
    {
        private IAccountsService Accounts { get; }

        private IReadingsCsvParserService Parser { get; }

        public AccountsController(
            IAccountsService accounts,
            IReadingsCsvParserService parser)
        {
            Accounts = accounts;
            Parser = parser;
        }

        [HttpGet]
        [Route("api/accounts")]
        public IEnumerable<Account> GetAccounts()
        {
            return Accounts.Accounts();
        }

        [HttpGet]
        [Route("api/accounts/{accountId}")]
        public Account GetAccount(int accountId)
        {
            var accounts = Accounts.Accounts();

            return Prelude.Try(() => accounts.First(a => a.ID == accountId))
                .Match(
                    Succ: a => a,
                    Fail: _ => throw new AccountNotFoundException(
                        $"Account with id {accountId} not found"
                    )
                );
        }

        [HttpGet]
        [Route("api/accounts/readings")]
        public IEnumerable<Reading> GetReadings()
        {
            return Accounts.Readings();
        }

        [HttpGet]
        [Route("api/accounts/{accountId}/readings")]
        public IEnumerable<Reading> GetReadings(int accountId)
        {
            return Accounts.Readings(accountId);
        }

        [HttpPost]
        [Route("api/accounts/readings")]
        public ImportReadingsResult ImportReadings([FromBody] ImportReadingsRequest request)
        {
            if (string.IsNullOrEmpty(request.Csv))
            {
                throw new NullOrEmptyCsvException();
            }

            var parsed = Parser.Parse(request.Csv);            
            var result = new ImportReadingsResult
            {
                Errors = ParseErrors(parsed.Errors)
            };

            foreach (var entry in parsed.Readings)
            {
                Prelude.Try(() => AddReading(entry))
                    .Match(
                        Succ: _ => result.Successful.Add(entry),
                        Fail: e => result.Errors.Add(CreateError(entry.Line, e))
                    );
            }

            return result;
        }

        private Unit AddReading(Entry entry)
        {
            Accounts.AddReading(
                entry.AccountId,
                entry.MeterReadingDateTime,
                entry.MeterReadValue
            );

            return Prelude.unit;
        }

        private List<ImportReadingsResult.Error> ParseErrors(IEnumerable<int> lines)
        {
            return lines.Map(line =>
                new ImportReadingsResult.Error
                {
                    Line = line,
                    Reason = ImportReadingsResult.Error.ReasonCode.InvalidEntry
                }
            ).ToList();
        }

        private ImportReadingsResult.Error CreateError(int line, Exception e)
        {
            return e switch
            {
                AccountNotFoundException _ => new ImportReadingsResult.Error
                {
                    Line = line,
                    Reason = ImportReadingsResult.Error.ReasonCode.AccountNotFound
                },
                DuplicateReadingException _ => new ImportReadingsResult.Error
                {
                    Line = line,
                    Reason = ImportReadingsResult.Error.ReasonCode.AlreadyExists
                },
                _ => throw new ArgumentException(),
            };
        }
    }

    public class AddReadingRequest
    {
        public DateTime Date { get; set; }
        public int Value { get; set; }
    }

    public class ImportReadingsRequest
    {
        public string Csv { get; set; }
    }

    public class ImportReadingsResult
    {
        public class Error
        {
            public enum ReasonCode
            {
                InvalidEntry,
                AlreadyExists,
                AccountNotFound
            }

            public int Line { get; set; }

            public ReasonCode Reason { get; set; }
        }

        public List<Entry> Successful { get; set; } = new List<Entry>();

        public List<Error> Errors { get; set; } = new List<Error>();
    }

    public class NullOrEmptyCsvException: Exception { }
}
