﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using LanguageExt;
using CsvHelper;
using MeterReading.Models.CSV.Reading;

namespace MeterReading.Services
{
    public class ReadingsParseResult
    {
        public List<Entry> Readings { get; set; } = new List<Entry>();
        public List<int> Errors { get; set; } = new List<int>();
    }

    public interface IReadingsCsvParserService
    {
        ReadingsParseResult Parse(string contents);
    }

    public class ReadingsCsvParserService: IReadingsCsvParserService
    {
        public ReadingsParseResult Parse(string contents)
        {
            using var reader = new StringReader(contents);
            using var csv = Csv(reader);

            return ReadContents(csv);
        }

        private CsvReader Csv(StringReader reader)
        {
            var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            csv.Configuration.RegisterClassMap<EntryMap>();

            return csv;
        }

        private ReadingsParseResult ReadContents(CsvReader csv)
        {
            var line = 2;
            var results = new ReadingsParseResult();

            csv.Read();
            csv.ReadHeader();
            csv.ValidateHeader<Entry>();

            while (csv.Read())
            {
                Prelude.Try(csv.GetRecord<Entry>)
                    .Match(
                        Succ: r => results.Readings.Add(new Entry
                        {
                            Line = line,
                            AccountId = r.AccountId,
                            MeterReadingDateTime = r.MeterReadingDateTime,
                            MeterReadValue = r.MeterReadValue
                        }),
                        Fail: e => results.Errors.Add(line)
                    );

                line++;
            }

            return results;
        }
    }
}
