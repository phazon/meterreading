﻿using MeterReading.Data;
using MeterReading.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MeterReading.Services
{
    public interface IAccountsService
    {
        IEnumerable<Account> Accounts();
        IEnumerable<Reading> Readings();
        IEnumerable<Reading> Readings(int accountId);

        void AddReading(int accountId, DateTime date, int value);
    }

    public class AccountsService: IAccountsService
    {
        private AccountsContext Context { get; }

        public AccountsService(AccountsContext context)
        {
            Context = context;
        }

        public IEnumerable<Account> Accounts()
        {
            return Context.Accounts;
        }

        public IEnumerable<Reading> Readings()
        {
            return Context.Readings;
        }

        public IEnumerable<Reading> Readings(int accountId)
        {
            if (!AccountExists(accountId))
            {
                throw AccountNotFound(accountId);
            }

            return Readings()
                .Where(r => r.AccountID == accountId);
        }

        public void AddReading(int accountId, DateTime date, int value)
        {
            if (!AccountExists(accountId))
            {
                throw AccountNotFound(accountId);
            }

            if (ReadingExists(accountId, date))
            {
                throw new DuplicateReadingException(
                    $"Reading at {date} already exists on account with ID {accountId}"
                );
            }

            Context.Readings.Add(new Reading
            {
               AccountID = accountId,
               Date = date,
               Value = value
            });

            Context.SaveChanges();
        }

        private bool AccountExists(int id) =>
            Context.Accounts.Any(a => a.ID == id);

        private bool ReadingExists(int accountId, DateTime date) =>
            Context.Readings.Any(r =>
                r.AccountID == accountId &&
                r.Date == date
            );

        private AccountNotFoundException AccountNotFound(int accountId) =>
            new AccountNotFoundException($"Account with id {accountId} not found");
    }

    public class AccountNotFoundException : Exception
    {
        public AccountNotFoundException()
        { }

        public AccountNotFoundException(string message) :
            base(message)
        { }
    }

    public class DuplicateReadingException : Exception
    {
        public DuplicateReadingException()
        { }
        public DuplicateReadingException(string message) :
            base(message)
        { }
    }
}
