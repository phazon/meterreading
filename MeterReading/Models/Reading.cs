﻿using System;

namespace MeterReading.Models
{
    public class Reading
    {
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public int Value { get; set; }

        public int AccountID { get; set; }
    }
}
