﻿using CsvHelper.Configuration;
using System;

namespace MeterReading.Models.CSV.Reading
{
    public class Entry
    {
        public int Line { get; set; }
        public int AccountId { get; set; }
        public DateTime MeterReadingDateTime { get; set; }
        public int MeterReadValue { get; set; }
    }

    public sealed class EntryMap : ClassMap<Entry>
    {
        public EntryMap()
        {
            Map(m => m.Line).Ignore();
            Map(m => m.AccountId);
            Map(m => m.MeterReadingDateTime)
                .TypeConverterOption.Format("dd/MM/yyyy HH:mm");
            Map(m => m.MeterReadValue);
        }
    }
}
