﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeterReading.Models
{
    public class Account
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<Reading> Readings { get; set; }
    }
}
