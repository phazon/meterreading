﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using MeterReading.Data;
using MeterReading.Models;

namespace AccountSeeder
{
    public class AccountEntry
    {
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    class Program
    {
        static void Main(string[] _)
        {
            using var reader = new StreamReader("Test_Accounts.csv");
            using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
            using var db = new AccountsContext();

            if (db.Accounts.Count() != 0)
            {
                Console.Write(
                    "Database is already seeded with accounts, aborting."
                );

                return;
            }

            foreach (var account in csv.GetRecords<AccountEntry>())
            {
                db.Add(new Account
                {
                    ID = account.AccountId,
                    FirstName = account.FirstName,
                    LastName = account.LastName,
                    Readings = new List<Reading>()
                });

                Console.WriteLine(
                    $"Added account for {account.FirstName} {account.LastName}" +
                    $"with ID {account.AccountId} to database."
                );
            }

            db.SaveChanges();
        }
    }
}
