using FluentAssertions;
using MeterReading.Data;
using MeterReading.Models;
using MeterReading.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{
    public class AccountsServiceTests: IDisposable
    {
        private AccountsService Service { get; }

        private AccountsContext Context { get; }

        private DateTime Now { get; } = DateTime.UtcNow;

        private IEnumerable<Account> Accounts { get; } =
            new List<Account>
            {
                new Account
                {
                    ID = 1,
                    FirstName = "Johnny",
                    LastName = "Test"
                },
                new Account
                {
                    ID = 2,
                    FirstName = "Dave",
                    LastName = "Test"
                }
            };

        private IEnumerable<Reading> Readings =>
            new List<Reading>
            {
                new Reading
                {
                    ID = 1,
                    Date = Now,
                    Value = 20,
                    AccountID = Accounts.ElementAt(0).ID
                },
                new Reading
                {
                    ID = 2,
                    Date = Now.AddSeconds(20),
                    Value = 15,
                    AccountID = Accounts.ElementAt(1).ID
                },
                new Reading
                {
                    ID = 3,
                    Date = Now.AddSeconds(-20),
                    Value = 10,
                    AccountID = Accounts.ElementAt(1).ID
                }
            };

        public AccountsServiceTests()
        {
            Context = new AccountsContext(options =>
                options.UseInMemoryDatabase("AccountsDatabase")
            );
            Context.Accounts.AddRange(Accounts);
            Context.Readings.AddRange(Readings);
            Context.SaveChanges();

            Service = new AccountsService(Context);
        }

        public void Dispose()
        {
            Context.Database.EnsureDeleted();
        }

        [Fact]
        public void ReturnsAllAccounta()
        {
            Service.Accounts().Should().Equal(Accounts);
        }

        [Fact]
        public void ReturnsAllReadings()
        {
            var result = Service.Readings();

            result.Should().BeEquivalentTo(Readings);
        }

        [Fact]
        public void ReturnsReadingsForAccountId()
        {
            foreach (var account in Accounts)
            {
                var readings = Readings
                    .Where(r => r.AccountID == account.ID);

                var result = Service.Readings(account.ID);

                result.Should().BeEquivalentTo(readings);
            }
        }

        [Fact]
        public void ReadingsThrowsIfAccountWithIdDoesNotExist()
        {
            var id = 404;

            Action act = () => Service.Readings(id);

            act.Should().Throw<AccountNotFoundException>()
                .WithMessage($"Account with id {id} not found");
        }

        [Fact]
        public void AddsReadingToAccountId()
        {
            foreach (var account in Accounts)
            {
                var expectedDate = DateTime.UtcNow;
                var expectedValue = 21;

                Service.AddReading(account.ID, expectedDate, expectedValue);

                Context.Readings.Should().Contain(r => 
                    r.AccountID == account.ID &&
                    r.Date == expectedDate &&
                    r.Value == expectedValue
                );
            }
        }

        [Fact]
        public void AddReadingThrowsIfAccountWithIdDoesNotExist()
        {
            var id = 404;

            Action act = () => Service.AddReading(id, DateTime.UtcNow, 21);

            act.Should().Throw<AccountNotFoundException>()
                .WithMessage($"Account with id {id} not found");
        }

        [Fact]
        public void AddReadingThrowsIfReadingAlreadyExistsOnAccount()
        {
            var account = Accounts.First();

            var date = DateTime.UtcNow;
            var value = 200;

            Service.AddReading(account.ID, date, value);

            Action act = () => Service.AddReading(account.ID, date, value);

            act.Should().Throw<DuplicateReadingException>().WithMessage(
                $"Reading at {date} already exists on account with ID {account.ID}"
            );
        }
    }
}
