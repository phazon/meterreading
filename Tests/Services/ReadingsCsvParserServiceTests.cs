﻿using CsvHelper;
using FluentAssertions;
using MeterReading.Models.CSV.Reading;
using MeterReading.Services;
using System;
using System.Globalization;
using Xunit;

namespace Tests.Services
{
    public class ReadingsCsvParserServiceTests
    {
        private ReadingsCsvParserService Parser { get; }

        private string Headers { get; } =
            "AccountId,MeterReadingDateTime,MeterReadValue";

        public ReadingsCsvParserServiceTests()
        {
            Parser = new ReadingsCsvParserService();
        }

        [Fact]
        public void ParseThrowsHeaderValidationExceptionIfRequiredHeadersMissing()
        {
            var csv = "2344,22/04/2019 09:24,1002";

            Action act = () => Parser.Parse(csv);

            act.Should().Throw<HeaderValidationException>();
        }

        [Fact]
        public void ParseAddsValidEntriesToResult()
        {
            var expectedReading = new Entry
            {
                Line = 3,
                AccountId = 1234,
                MeterReadingDateTime = DateTime.ParseExact(
                    "22/04/2019 09:24",
                    "dd/MM/yyyy HH:mm",
                    CultureInfo.InvariantCulture
                ),
                MeterReadValue = 1000
            };

            var csv = $"{Headers}"
                 + "\n1000,22/04/2019 09:24,VOID"
                 + "\n1234,22/04/2019 09:24,1000";

            var results = Parser.Parse(csv);
            results.Readings.Should().ContainEquivalentOf(expectedReading);
        }

        [Fact]
        public void ParseAddsInvalidEntriesToErrors()
        {
            var csv = $"{Headers}"
                 + "\n1234,22/04/2019 09:24,1000"
                 + "\n1000,22/04/2019 09:24,VOID";

            var results = Parser.Parse(csv);
            results.Errors.Should().Contain(3);
        }
    }
}
