﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using NSubstitute;
using Xunit;
using MeterReading.Controllers;
using MeterReading.Models;
using MeterReading.Services;
using NSubstitute.ExceptionExtensions;

namespace Tests.Controllers
{
    public class AccountsControllerTests
    {
        private AccountsController Controller { get; }

        private IAccountsService Accounts { get; } =
            Substitute.For<IAccountsService>();

        private IReadingsCsvParserService Parser { get; } =
            Substitute.For<IReadingsCsvParserService>();

        private String CsvHeaders { get; } =
            "AccountId,MeterReadingDateTime,MeterReadValue";

        public AccountsControllerTests()
        {
            Controller = new AccountsController(Accounts, Parser);
        }

        [Fact]
        public void ReturnsAllAccounts()
        {
            var expectedAccounts = new List<Account>
            {
                new Account
                {
                    ID = 1,
                    FirstName = "Johnny",
                    LastName = "Test"
                }
            };

            Accounts.Accounts().Returns(expectedAccounts);

            Controller.GetAccounts().Should().BeEquivalentTo(expectedAccounts);
        }

        [Fact]
        public void ReturnsAccountWithId()
        {
            var expectedAccount = new Account
            {
                ID = 2,
                FirstName = "Johnny",
                LastName = "Test"
            };

            Accounts.Accounts().Returns(
                new List<Account>
                {
                    new Account
                    {
                        ID = 1,
                        FirstName = "Timmy",
                        LastName = "Test"
                    },
                    expectedAccount
                }
            );

            Controller.GetAccount(expectedAccount.ID)
                .Should().Be(expectedAccount);
        }

        [Fact]
        public void GetAccountThrowsIfAccountDoesNotExist()
        {
            Accounts.Accounts().Returns(
                new List<Account>
                {
                    new Account
                    {
                        ID = 1,
                        FirstName = "Timmy",
                        LastName = "Test"
                    }
                }
            );

            var id = 2;

            Action act = () => Controller.GetAccount(id);

            act.Should().Throw<AccountNotFoundException>()
                .WithMessage($"Account with id {id} not found");
        }

        [Fact]
        public void ReturnsAllReadings()
        {
            var expectedReadings = new List<Reading>
            {
                new Reading
                {
                    ID = 1,
                    Date = DateTime.UtcNow,
                    Value = 21,
                    AccountID = 2
                },
                new Reading
                {
                    ID = 2,
                    Date = DateTime.UtcNow,
                    Value = 210,
                    AccountID = 1
                }
            };

            Accounts.Readings().Returns(expectedReadings);

            Controller.GetReadings().Should().BeEquivalentTo(expectedReadings);
        }

        [Fact]
        public void ReturnsReadingsForAccountWithId()
        {
            var expectedReadings = new List<Reading>
            {
                new Reading
                {
                    ID = 1,
                    Date = DateTime.UtcNow,
                    Value = 21,
                    AccountID = 2
                }
            };

            Accounts.Readings(Arg.Any<int>()).Returns(expectedReadings);

            Controller.GetReadings(2).Should().BeEquivalentTo(expectedReadings);
        }

        [Fact]
        public void ImportReadingsThrowsIfCsvEmpty()
        {
            Action act = () => 
                Controller.ImportReadings(new ImportReadingsRequest
                {
                    Csv = ""
                });

            act.Should().Throw<NullOrEmptyCsvException>();
        }

        [Fact]
        public void ImportReadingsAddsInvalidEntriesToResult()
        {
            var csv = CsvHeaders
                + "\n1000,22/04/2019 09:24,200"
                + "\n2233,22/04/2019 12:25,323"
                + "\n1234,22/04/2019 12:25,VOID"
                + "\nbroken,22/04/2019 12:25,333";

            var parsed = Parse(csv);
            Parser.Parse(Arg.Any<string>()).Returns(parsed);

            var expectedErrors = parsed.Errors.Map(line =>
                new ImportReadingsResult.Error
                {
                    Line = line,
                    Reason = ImportReadingsResult.Error.ReasonCode.InvalidEntry
                }
            );

            var result = Controller.ImportReadings(new ImportReadingsRequest
            {
                Csv = csv
            });

            result.Errors.Should().BeEquivalentTo(expectedErrors);
        }

        [Fact]
        public void ImportReadingsAddsValidEntriesToAccounts()
        {
            var csv = CsvHeaders
                + "\n1000,22/04/2019 09:24,200"
                + "\n2233,22/04/2019 12:25,323"
                + "\n1234,22/04/2019 12:25,VOID";

            var parsed = Parse(csv);
            Parser.Parse(Arg.Any<string>()).Returns(parsed);

            Controller.ImportReadings(new ImportReadingsRequest
            {
                Csv = csv
            });

            foreach (var entry in parsed.Readings)
            {
                Accounts.Received(1).AddReading(
                    entry.AccountId,
                    entry.MeterReadingDateTime,
                    entry.MeterReadValue
                );
            }
        }        

        [Fact]
        public void ImportReadingsAddsSuccesfullyAddedEntriesToResult()
        {
            var csv = CsvHeaders
                + "\n1000,22/04/2019 09:24,200"
                + "\n2233,22/04/2019 12:25,323"
                + "\n1234,22/04/2019 12:25,VOID";

            var parsed = Parse(csv);
            Parser.Parse(Arg.Any<string>()).Returns(parsed);

            var result = Controller.ImportReadings(
                new ImportReadingsRequest
                {
                    Csv = csv
                }
            );

            result.Successful.Should().BeEquivalentTo(parsed.Readings);
        }

        [Fact]
        public void ImportReadingsAddsUnsuccesfullyAddedEntriesToResult()
        {
            var csv = CsvHeaders
                + "\n1000,22/04/2019 09:24,200"
                + "\n2233,22/04/2019 12:25,323"
                + "\n2233,22/04/2019 12:25,454"  // duplicate entry
                + "\n5555,22/04/2019 12:25,123"; // id doesn't exist

            var parsed = Parse(csv);
            Parser.Parse(Arg.Any<string>()).Returns(parsed);

            Accounts.When(a => a.AddReading(5555, Arg.Any<DateTime>(), Arg.Any<int>()))
                .Do(_ => throw new AccountNotFoundException("not found"));

            Accounts.When(a => a.AddReading(2233, Arg.Any<DateTime>(), 454))
                .Do(_ => throw new DuplicateReadingException());

            var result = Controller.ImportReadings(
                new ImportReadingsRequest
                {
                    Csv = csv
                }
            );

            result.Errors.Should().BeEquivalentTo(
                new List<ImportReadingsResult.Error>
                {
                    new ImportReadingsResult.Error
                    {
                        Line = 4,
                        Reason = ImportReadingsResult.Error.ReasonCode.AlreadyExists,
                    },
                    new ImportReadingsResult.Error
                    {
                        Line = 5,
                        Reason = ImportReadingsResult.Error.ReasonCode.AccountNotFound,
                    }
                }
            );
        }

        public ReadingsParseResult Parse(string csv)
        {
            return new ReadingsCsvParserService().Parse(csv);
        }
    }
}
